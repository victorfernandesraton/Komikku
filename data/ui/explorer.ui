<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2019-2023 Valéry Febvre

This file is part of Komikku.

Komikku is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Komikku is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Komikku.  If not, see <http://www.gnu.org/licenses/>.
-->
<interface>
  <requires lib="gtk" version="4.0"/>
  <requires lib="libadwaita" version="1.0"/>
  <template class="Explorer_" parent="GtkStack">
    <property name="transition_type">slide-left-right</property>
    <child>
      <object class="GtkStackPage">
        <property name="name">servers</property>
        <property name="child">
          <object class="GtkBox">
            <property name="orientation">vertical</property>
            <child>
              <object class="AdwClamp">
                <property name="maximum_size">768</property>
                <property name="margin-end">12</property>
                <property name="margin-start">12</property>
                <child>
                  <object class="GtkSearchBar" id="servers_page_searchbar">
                    <child>
                      <object class="GtkSearchEntry" id="servers_page_searchentry">
                        <property name="placeholder-text" translatable="yes">Search for servers by name or language</property>
                        <property name="hexpand">1</property>
                      </object>
                    </child>
                    <style>
                      <class name="inline"/>
                    </style>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="GtkSeparator" id="servers_page_searchbar_separator"/>
            </child>
            <child>
              <object class="GtkScrolledWindow">
                <property name="vexpand">1</property>
                <property name="hscrollbar_policy">never</property>
                <property name="child">
                  <object class="GtkViewport">
                    <property name="scroll-to-focus">True</property>
                    <child>
                      <object class="AdwClamp">
                        <property name="maximum_size">768</property>
                        <property name="margin-top">12</property>
                        <property name="margin-end">12</property>
                        <property name="margin-bottom">12</property>
                        <property name="margin-start">12</property>
                        <child>
                          <object class="GtkBox">
                            <property name="orientation">vertical</property>
                            <property name="spacing">32</property>
                            <child>
                              <object class="GtkListBox" id="servers_page_pinned_listbox">
                                <property name="name">pinned_servers</property>
                                <property name="selection_mode">none</property>
                                <style>
                                  <class name="boxed-list"/>
                                </style>
                              </object>
                            </child>
                            <child>
                              <object class="GtkListBox" id="servers_page_listbox">
                                <property name="name">servers</property>
                                <property name="selection_mode">none</property>
                                <style>
                                  <class name="boxed-list"/>
                                </style>
                              </object>
                            </child>
                          </object>
                        </child>
                      </object>
                    </child>
                  </object>
                </property>
              </object>
            </child>
          </object>
        </property>
      </object>
    </child>
    <child>
      <object class="GtkStackPage">
        <property name="name">search</property>
        <property name="child">
          <object class="GtkBox">
            <property name="orientation">vertical</property>
            <child>
              <object class="AdwViewStack" id="search_page_stack">
                <child>
                  <object class="AdwViewStackPage">
                    <property name="name">search</property>
                    <property name="title" translatable="yes">Search</property>
                    <property name="icon_name">system-search-symbolic</property>
                    <property name="child">
                      <object class="GtkBox">
                        <property name="orientation">vertical</property>
                        <child>
                          <object class="AdwClamp">
                            <property name="maximum_size">768</property>
                            <property name="margin-end">12</property>
                            <property name="margin-start">12</property>
                            <child>
                              <object class="GtkSearchBar" id="search_page_searchbar">
                                <property name="search_mode_enabled">True</property>
                                <child>
                                  <object class="GtkBox">
                                    <property name="hexpand">1</property>
                                    <child>
                                      <object class="GtkSearchEntry" id="search_page_searchentry">
                                        <property name="hexpand">1</property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkMenuButton" id="search_page_filter_menu_button">
                                        <property name="focus_on_click">0</property>
                                        <property name="receives_default">1</property>
                                        <property name="icon-name">go-down-symbolic</property>
                                      </object>
                                    </child>
                                    <style>
                                      <class name="linked"/>
                                    </style>
                                  </object>
                                </child>
                                <style>
                                  <class name="inline"/>
                                </style>
                              </object>
                            </child>
                          </object>
                        </child>
                        <child>
                          <object class="GtkSeparator"/>
                        </child>
                        <child>
                          <object class="GtkScrolledWindow">
                            <property name="vexpand">1</property>
                            <property name="hscrollbar_policy">never</property>
                            <property name="child">
                              <object class="GtkViewport">
                                <property name="scroll-to-focus">True</property>
                                <child>
                                  <object class="AdwClamp">
                                    <property name="maximum_size">768</property>
                                    <property name="margin-top">12</property>
                                    <property name="margin-end">12</property>
                                    <property name="margin-bottom">12</property>
                                    <property name="margin-start">12</property>
                                    <child>
                                      <object class="GtkStack" id="search_page_search_stack">
                                        <property name="vhomogeneous">0</property>
                                        <child>
                                          <object class="GtkStackPage">
                                            <property name="name">results</property>
                                            <property name="child">
                                              <object class="AdwBin">
                                                <child>
                                                  <object class="GtkListBox" id="search_page_search_listbox">
                                                    <property name="valign">start</property>
                                                    <property name="selection_mode">none</property>
                                                    <style>
                                                      <class name="boxed-list"/>
                                                    </style>
                                                  </object>
                                                </child>
                                              </object>
                                            </property>
                                          </object>
                                        </child>
                                        <child>
                                          <object class="GtkStackPage">
                                            <property name="name">no_results</property>
                                            <property name="child">
                                              <object class="AdwStatusPage" id="search_page_search_no_results_status_page">
                                                <property name="icon-name">computer-fail-symbolic</property>
                                              </object>
                                            </property>
                                          </object>
                                        </child>
                                        <child>
                                          <object class="GtkStackPage">
                                            <property name="name">loading</property>
                                            <property name="child">
                                              <object class="GtkBox">
                                                <property name="orientation">vertical</property>
                                                <property name="valign">center</property>
                                                <child>
                                                  <object class="GtkSpinner" id="search_page_search_spinner">
                                                    <property name="height-request">50</property>
                                                    <property name="width-request">50</property>
                                                    <property name="visible">True</property>
                                                    <style>
                                                      <class name="spinner"/>
                                                    </style>
                                                  </object>
                                                </child>
                                              </object>
                                            </property>
                                          </object>
                                        </child>
                                        <child>
                                          <object class="GtkStackPage">
                                            <property name="name">intro</property>
                                            <property name="child">
                                              <object class="AdwStatusPage" id="search_page_search_intro_status_page">
                                                <property name="icon-name">system-search-symbolic</property>
                                              </object>
                                            </property>
                                          </object>
                                        </child>
                                      </object>
                                    </child>
                                  </object>
                                </child>
                              </object>
                            </property>
                          </object>
                        </child>
                      </object>
                    </property>
                  </object>
                </child>
                <child>
                  <object class="AdwViewStackPage">
                    <property name="name">most_populars</property>
                    <property name="title" translatable="yes">Most Popular</property>
                    <property name="icon_name">starred-symbolic</property>
                    <property name="child">
                      <object class="GtkScrolledWindow">
                        <property name="vexpand">1</property>
                        <property name="hscrollbar_policy">never</property>
                        <property name="child">
                          <object class="GtkViewport">
                            <property name="scroll-to-focus">True</property>
                            <child>
                              <object class="AdwClamp">
                                <property name="maximum_size">768</property>
                                <property name="margin-top">12</property>
                                <property name="margin-end">12</property>
                                <property name="margin-bottom">12</property>
                                <property name="margin-start">12</property>
                                <child>
                                  <object class="GtkStack" id="search_page_most_populars_stack">
                                    <property name="vhomogeneous">0</property>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">results</property>
                                        <property name="child">
                                          <object class="AdwBin">
                                            <child>
                                              <object class="GtkListBox" id="search_page_most_populars_listbox">
                                                <property name="valign">start</property>
                                                <property name="selection_mode">none</property>
                                                <style>
                                                  <class name="boxed-list"/>
                                                </style>
                                              </object>
                                            </child>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">no_results</property>
                                        <property name="child">
                                          <object class="AdwStatusPage" id="search_page_most_populars_no_results_status_page">
                                            <property name="icon-name">computer-fail-symbolic</property>
                                            <property name="child">
                                              <object class="GtkButton">
                                                <property name="label" translatable="yes">Retry</property>
                                                <property name="halign">center</property>
                                                <style>
                                                  <class name="pill"/>
                                                  <class name="suggested-action"/>
                                                </style>
                                              </object>
                                            </property>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">loading</property>
                                        <property name="child">
                                          <object class="GtkBox">
                                            <property name="orientation">vertical</property>
                                            <property name="valign">center</property>
                                            <child>
                                              <object class="GtkSpinner" id="search_page_most_populars_spinner">
                                                <property name="height-request">50</property>
                                                <property name="width-request">50</property>
                                                <property name="visible">True</property>
                                                <style>
                                                  <class name="spinner"/>
                                                </style>
                                              </object>
                                            </child>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                  </object>
                                </child>
                              </object>
                            </child>
                          </object>
                        </property>
                      </object>
                    </property>
                  </object>
                </child>
                <child>
                  <object class="AdwViewStackPage">
                    <property name="name">latest_updates</property>
                    <property name="title" translatable="yes">Latest Updates</property>
                    <property name="icon_name">software-update-available-symbolic</property>
                    <property name="child">
                      <object class="GtkScrolledWindow">
                        <property name="vexpand">1</property>
                        <property name="hscrollbar_policy">never</property>
                        <property name="child">
                          <object class="GtkViewport">
                            <property name="scroll-to-focus">True</property>
                            <child>
                              <object class="AdwClamp">
                                <property name="maximum_size">768</property>
                                <property name="margin-top">12</property>
                                <property name="margin-end">12</property>
                                <property name="margin-bottom">12</property>
                                <property name="margin-start">12</property>
                                <child>
                                  <object class="GtkStack" id="search_page_latest_updates_stack">
                                    <property name="vhomogeneous">0</property>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">results</property>
                                        <property name="child">
                                          <object class="AdwBin">
                                            <child>
                                              <object class="GtkListBox" id="search_page_latest_updates_listbox">
                                                <property name="valign">start</property>
                                                <property name="selection_mode">none</property>
                                                <style>
                                                  <class name="boxed-list"/>
                                                </style>
                                              </object>
                                            </child>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">no_results</property>
                                        <property name="child">
                                          <object class="AdwStatusPage" id="search_page_latest_updates_no_results_status_page">
                                            <property name="icon-name">computer-fail-symbolic</property>
                                            <property name="child">
                                              <object class="GtkButton">
                                                <property name="label" translatable="yes">Retry</property>
                                                <property name="halign">center</property>
                                                <style>
                                                  <class name="pill"/>
                                                  <class name="suggested-action"/>
                                                </style>
                                              </object>
                                            </property>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                    <child>
                                      <object class="GtkStackPage">
                                        <property name="name">loading</property>
                                        <property name="child">
                                          <object class="GtkBox">
                                            <property name="orientation">vertical</property>
                                            <property name="valign">center</property>
                                            <child>
                                              <object class="GtkSpinner" id="search_page_latest_updates_spinner">
                                                <property name="height-request">50</property>
                                                <property name="width-request">50</property>
                                                <property name="visible">True</property>
                                                <style>
                                                  <class name="spinner"/>
                                                </style>
                                              </object>
                                            </child>
                                          </object>
                                        </property>
                                      </object>
                                    </child>
                                  </object>
                                </child>
                              </object>
                            </child>
                          </object>
                        </property>
                      </object>
                    </property>
                  </object>
                </child>
              </object>
            </child>
            <child>
              <object class="AdwViewSwitcherBar" id="search_page_viewswitcherbar">
                <property name="stack">search_page_stack</property>
              </object>
            </child>
          </object>
        </property>
      </object>
    </child>
  </template>
</interface>
